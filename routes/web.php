<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//2. Servicio que liste todos los usuarios existentes
Route::get('/usuarios', 'HomeController@usuarios')->name('usuarios');


//3. Usuarios operadores
Route::get('/operadores', 'HomeController@operadores')->name('operadores');


//5. Usuarios activos
Route::get('/activos', 'HomeController@activos')->name('activos');


//6. Usuarios inactivos
Route::get('/inactivos', 'HomeController@inactivos')->name('inactivos');

//7. Usuario con identificador
Route::get('/usuario/{id}', 'HomeController@usuario')->name('usuario');