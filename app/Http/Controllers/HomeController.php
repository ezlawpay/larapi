<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Rol;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function usuarios()
        {
            // $this->authorize($user);
            return $users = User::all();
        }

    public function operadores()
        {
            return Rol::with('user')->where('id',2)->get();
        }



    public function activos()
        {
            return User::where('activo',1)->get();
        }

    public function inactivos()
        {
            return User::where('activo',0)->get();
        }

        public function usuario($id)
        {

            return User::where('id',$id)->get();
        }

}
