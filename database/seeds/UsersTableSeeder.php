<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

	        DB::table('users')->insert([
	        	'username' => 'eduar',
	        	'email' => 'eduar@gmail.com',
	            'name' => 'Eduardo',
	            'paternal_surname' => 'Gomez',
	            'maternal_surname' => 'Jimenez',
	            'age' => '29',
	            'rol_id' => '1',
	            'password' => bcrypt('password'),
	            'activo' => 1
	        ]);
	        DB::table('users')->insert([
	        	'username' => 'edu',
	        	'email' => 'edu@gmail.com',
	            'name' => 'Eduardo',
	            'paternal_surname' => 'Gomez',
	            'maternal_surname' => 'Jimenez',
	            'age' => '29',
	            'rol_id' => '2',
	            'password' => bcrypt('password'),
	            'activo' => 0
	        ]);
	        DB::table('users')->insert([
	        	'username' => 'moni',
	        	'email' => 'moni@gmail.com',
	            'name' => 'Monica',
	            'paternal_surname' => 'Paredes',
	            'maternal_surname' => 'Paredes',
	            'age' => '29',
	            'rol_id' => '3',
	            'password' => bcrypt('password'),
	            'activo' => 1
	        ]);
	       	DB::table('users')->insert([
	        	'username' => 'paco',
	        	'email' => 'paco@gmail.com',
	            'name' => 'Francisco',
	            'paternal_surname' => 'Bedolla',
	            'maternal_surname' => 'Bedolla',
	            'age' => '29',
	            'rol_id' => '3',
	            'password' => bcrypt('password'),
	            'activo' => 0
	        ]);
	       	DB::table('users')->insert([
	        	'username' => 'javi',
	        	'email' => 'javi@gmail.com',
	            'name' => 'Javier',
	            'paternal_surname' => 'Duarte',
	            'maternal_surname' => 'Duarte',
	            'age' => '29',
	            'rol_id' => '2',
	            'password' => bcrypt('password'),
	            'activo' => 1
	        ]);
    }
}
